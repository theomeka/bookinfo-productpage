# How to run product page

## Prerequisite

* Python 3.8

```bash
pip install -r requirements.txt
python productpage.py 9080
```


## test in dokcer
``` 
docker build -t productpage .
docker run -p 9080:9080 productpage
```